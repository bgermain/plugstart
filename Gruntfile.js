module.exports = function(grunt) {

    grunt.initConfig({
        project: {
            name: "plugstart"
        },
        uglify: {
            def: {
                files: {
                    '<%= project.name %>.min.js': 'src/<%= project.name %>.js'
                }
            }
        },
        // clean: {
        //     docs: ['docs/']
        // },
        // jsdoc: {
        //     src: 'src/<%= project.name %>.js',
        //     options: {
        //         // configure: '.jsdocrc',
        //         destination: 'docs'
        //     }
        // },
        watch: {
            def: {
                files: ['src/*.js'],
                tasks: ['uglify'],
                options: {
                    spawn: false,
                    livereload: {
                        port: 9000
                    }
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    // grunt.loadNpmTasks('grunt-jsdoc');

    grunt.registerTask('default', ['watch:def']);
    grunt.registerTask('js-task', ['uglify']);
    // grunt.registerTask('docs', ['clean', 'jsdoc']);

};