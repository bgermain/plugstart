!(function($) {

  // Declaration de notre plugin
  $.initComponents = function(options) {

      var plugin = this;
      plugin.options = {}

      plugin.init = function() {
        // on stocke les options dans un objet 
        plugin.options = $.extend({}, options)
        // lecture et initialisation des composants 
        plugin.components = (function(){
          var $components = $('[data-component]'),
              dataComponents = {}
              dataComponents.cpNodeInit = false
          //des composant dans la page ?    
          if($components.length>0) {
            for(i=0, len=$components.length;i<len;i++){
              
              var cpCfg    = $components[i].getAttribute('data-component'),
                  $cpNode  = $($components[i]),
                  cpId     = cpCfg.split(':[')[0],
                  cpName   = cpId.split("-")[0],
                  cpParams = cpCfg.split(':[')[1].slice(0,-1)
              //le composant est déjà traité ? 
              if($cpNode.data("init-"+cpId) === undefined) {
                dataComponents.cpNodeInit = true
                if(typeof dataComponents[cpName] != "object") {
                  dataComponents[cpName] = {}
                }
                dataComponents[cpName][cpId] = {}
                dataComponents[cpName][cpId].idCps  = cpId
                dataComponents[cpName][cpId].param  = JSON.parse(cpParams)
                dataComponents[cpName][cpId].cpNode = $cpNode
              }
            }
            return dataComponents
          }else{
            //pas de composant dans la page
            return false
          }
        })()
        //Des composants dans la page et non déjà traité ?
        if(plugin.components && plugin.components.cpNodeInit) {
          plugin.start();
        }
      }
      plugin.start = function() {
        for (var cps in plugin.components) {
          for(var cpsId in plugin.components[cps]) {
            //on éxécute le composant $("element").composant(param)
            plugin.components[cpsId] = $(plugin.components[cps][cpsId].cpNode)[cps](plugin.components[cps][cpsId].param)
            //on enregistre l'état déjà initialisé
            $(plugin.components[cps][cpsId].cpNode).data("init-"+cpsId,true)
            //Il y a un callbal attaché au composant ?              
            if(plugin.options.addCallback) {
              for(var key in plugin.options.addCallback) {
                //oui y'a un callback, on éxécute
                if( key == cpsId && typeof fnCallbacks[plugin.options.addCallback[key]] === "function") {
                  fnCallbacks[plugin.options.addCallback[key]]()
                }
              }
            }
          }
        }
      }
      
      plugin.init();
  }

  $.fn.initComponents = function(options) {
      var plugin = new $.initComponents(options)
      //$(this).data('initComponents', plugin)
      return plugin.components;
  };

})(window.Zepto || window.jQuery);
