/**
 * PLUGStart
 * v 0.0.2
 *
 * A tentative of plugin that can start any Jquery/Zepto plugin
 * from a simple data attribute in the HTML document.
 *
 * CHANGELOG
 * v 0.0.1 :
 * - initial version
 *
 * v 0.0.2 :
 * - removed the reviveParams method
 * - optimization of the plugin facade.
 *
 * TODO
 * - refactoring
 * - optimizations for the start loop
 * - optimize collect
 */

(function($) {
    'use strict';
    var pluginName = 'plugStart',
        version = '0.0.2';

    /**
     * @description Revive functions a JSON object
     * @param  {String} str - A string representing the JSON
     * @param  {Object} context - Function context (this)
     * @return {Object} - The rebuilt JSON
     */
    var revive = function(str, context) {
        return JSON.parse(str, function(k, v) {
            if (typeof context.fns[v] === 'function') {
                return context.fns[v];
            } else {
                return v;
            }
        });
    };


    /**
     * @description PlugStart constructor
     * @constructor
     * @param {String} method - Name of the method from the prototype
     * @param {Array} args - The method arguments
     */
    $[pluginName] = function(method, args) {
        // constructor body
        this._name = pluginName;
        this._version = version;
        this.debug = false;
        this.components = this.collect();
        this.fns = {};
    }

    $[pluginName].prototype = {
        /**
         * @description Collect the components initialization in the page
         * @return {Object|Boolean} - an object containing all the collected components or false if no component
         */
        collect: function() {
            // method body
            var $components = $('[data-component]'),
                dataComponents = {};

            // dataComponents.alive = false;
            // First, we look for all the components in the page
            if ($components.length > 0) {
                for (var i = 0, len = $components.length; i < len; i++) {
                    var cpCfg = $components[i].getAttribute('data-component').split(':['),
                        $node = $($components[i]),
                        cpId = cpCfg[0],
                        cpName = cpId.split("-")[0],
                        cpParams = cpCfg[1].slice(0, -1);

                    // Is the component already initialized ?
                    if ($node.data("init-" + cpId) === undefined) {
                        if (typeof dataComponents[cpName] != "object") {
                            dataComponents[cpName] = {};
                        }
                        dataComponents[cpName][cpId] = {};
                        dataComponents[cpName][cpId].id = cpId;
                        dataComponents[cpName][cpId].params = cpParams;
                        dataComponents[cpName][cpId].node = $node;
                    }
                }
                return dataComponents;
            } else {
                // If no components in the page we stop
                return false;
            }
        },
        /**
         * @description Get an intanciated component
         * @param  {String} id - The id of the component to get
         * @return {Object} - The instanciated component
         */
        getPlugin: function(id) {
            this.debug && console.log('getting Plugin ' + id, this[id]);

            return this[id];
        },
        /**
         * @description Register a callback method for a component
         * @param  {Object|String} fn - An object containing the callback functions or the name of the callback
         * @param  {Function} callback - The callback function
         * @return {Object} - PlugStart object
         */
        register: function(fn, callback) {
            this.debug && console.log('calling register');

            if (Object.prototype.toString.call(fn) === '[object Object]') {
                this.debug && console.log('registering : ', fn);
                $.extend(this.fns, fn);
            } else if (typeof fn === 'string' && typeof callback === 'function') {
                this.debug && console.log('registering this function : ', fn, ' = ', callback.toString());
                if (!(fn in this.fns)) this.fns[fn] = callback;
            }

            return this;
        },
        // reviveParams: function() {
        //     var components = this.components,
        //         aCpsKeys = Object.keys(components), // get all keys in the components object
        //         i = 0,
        //         cpsLen = aCpsKeys.length;

        //     while (i < cpsLen) {
        //         var componentType = components[aCpsKeys[i]],
        //             aCpKeys = Object.keys(componentType), // get all components type
        //             j = 0,
        //             cpLen = aCpKeys.length;

        //         while (j < cpLen) {
        //             var k = aCpKeys[i],
        //                 component = componentType[aCpKeys[j]]; // get the component

        //             if (component.hasOwnProperty('params')) component.params = revive(component.params, this);
        //             j++;
        //         }
        //         i++;
        //     }
        // },
        /**
         * @description Start all the components in the page
         * @return {Object} - PlugStart object
         */
        start: function() {
            console.time('Plugstart start method');
            this.debug && console.log('starting all components from ', this.components);

            // first we collect all the Components of the page to refresh the object
            var components = this.collect();
            $.extend(true, this.components, components);

            for (var cps in components) {
                for (var cpsId in components[cps]) {
                    var oComponent = components[cps][cpsId],
                        componentNode = components[cps][cpsId].node,
                        componentParams = components[cps][cpsId].params;

                    this.debug && console.log(componentNode, cps);

                    // We revive the component callback in params and the params object from the string
                    if (oComponent.hasOwnProperty('params') && typeof componentParams === 'string') oComponent.params = revive(oComponent.params, this);

                    // Time to start the component with $("element").composant(params)
                    // but before we need to check if the component is linked to the page ...
                    if (typeof componentNode[cps] === 'function') {
                        this[cpsId] = componentNode[cps](oComponent.params);
                        // Store the live status of the components
                        oComponent.alive = true;
                        componentNode.data("init-" + cpsId, true);
                    } else {
                        console.warn('The plugin ' + cps + ' does not exist, please link it first.');
                    }
                }
            }

            console.timeEnd('Plugstart start method');
            return this;
        }
    };


    /**
     * @description PlugStart plugin
     * @param  {String} method - Name of the public method to fire
     * @param  {Array} args - The method arguments
     * @return {Object} - PlugStart or specific plugin object
     */
    $.fn[pluginName] = function(method, args) {
        console.time('Plugstart');
        var $b = $('body'),
            instance = $b.data(pluginName) ? $b.data(pluginName) : false;

        if (!instance) {
            // We create an instance of PlugStart if there no exiting one
            var myPlugin = new $[pluginName]();

            // we attach the PlugStart object to the document body
            $b.data(pluginName, myPlugin);
            return myPlugin;
        }

        // We apply the public methods of $.fn.plugStart : start, register and getPlugin
        console.timeEnd('Plugstart');
        if (method) return instance[method].apply(instance, args);
        else return instance;
    };

    // Autostart the plugStart
    // You can then directly use the API
    $.fn[pluginName]();

}(Zepto || jQuery));